FROM alpine:latest

RUN apk add bash iproute2 dnsmasq

COPY root/ /

ENTRYPOINT ["/startup.sh"]
