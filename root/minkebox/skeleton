{
  name: `Shaped Network`,
  description: `Create a network then route, shape and limit traffic crossing to a target network.`,
  image: `registry.minkebox.net/minkebox/networkshaper`,
  uuid: `06DB17F2-1FD1-4142-AE13-6CA3F10DB5B8`,
  tags: [
    `Networking`
  ],
  actions: [
    {
      type: `Header`,
      title: `Configure`
    },
    {
      type: `SelectNetwork`,
      name: `primary`,
      description: `Select target network`
    },
    {
      type: `EditEnvironment`,
      name: `IN_TO_OUT`,
      description: `Maximum bandwidth out to target network (Kb/s)`
    },
    {
      type: `EditEnvironment`,
      name: `OUT_TO_IN`,
      description: `Maximum bandwidth in from target network (Kb/s)`
    },
     {
      type: `EditEnvironmentAsCheckbox`,
      name: `INTERNET_ONLY`,
      description: `Clients can <b>only</b> access the Internet`
    },
  ],
  properties: [
    {
      type: `Feature`,
      name: `+NET_ADMIN`
    },
    {
      type: `Environment`,
      name: `IN_TO_OUT`
    },
    {
      type: `Environment`,
      name: `OUT_TO_IN`
    },
    {
      type: `Environment`,
      name: `INTERNET_ONLY`,
      value: false
    },
    {
      type: `Network`,
      name: `primary`,
      value: `home`
    },
    {
      type: `Network`,
      name: `secondary`,
      value: `__create`
    }
  ],
  monitor: {
    cmd: `echo $(iptables -L RX -x -v -n | awk 'NR == 3 {print $2}') $(iptables -L TX -x -v -n | awk 'NR == 3 {print $2}')`,
    target: `helper`,
    init: `
      <div style="min-width: 400px; height: 250px">
        <canvas style="position: absolute" id="{{ID}}"></canvas>
      </div>
      <script>
        (function(){
          const chart = new Chart(document.getElementById("{{ID}}").getContext("2d"), {
            type: 'line',
            data: {
              labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63],
              datasets: [
                { data: [ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ], label: 'RX', borderColor: '#88cce7', backgroundColor: '#88cce7', fill: false, pointRadius: 0, clip: { top: 0, right: -10, bottom: 0, left: -10 } },
                { data: [ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ], label: 'TX', borderColor: '#41b376', backgroundColor: '#41b376', fill: false, pointRadius: 0, clip: { top: 0, right: -10, bottom: 0, left: -10 } }
              ]
            },
            options: {
              animation: { duration: 1000, easing: 'linear' },
              maintainAspectRatio: false,
              adaptive: true,
              title: { display: true, text: 'Bandwidth (Mb/s)' },
              scales: {
                xAxes: [{
                  display: false
                }],
                yAxes: [{
                  ticks: { beginAtZero: true }
                }]
              }
            }
          });
          const state = {
            last: [ 0, 0 ],
            then: 0
          };
          window.monitor("{{ID}}", 1, (input) => {
            const rxtx = input.split(' ');
            if (rxtx.length == 2) {
              const now = Date.now() / 1000;
              rxtx[0] = parseInt(rxtx[0]);
              rxtx[1] = parseInt(rxtx[1]);
              let elapse = Math.min(chart.data.datasets[0].data.length, Math.floor(now - state.then));
              if (elapse > 5) {
                if (elapse >= chart.data.datasets[0].data.length) {
                  state.last = rxtx;
                }
                for (; elapse > 0; elapse--) {
                  chart.data.datasets[0].data.shift();
                  chart.data.datasets[1].data.shift();
                  chart.data.datasets[0].data.push(0);
                  chart.data.datasets[1].data.push(0);
                }
              }
              chart.data.datasets[0].data.shift();
              chart.data.datasets[1].data.shift();
              chart.data.datasets[0].data.push((rxtx[0] - state.last[0]) * 8 / 1000000 / (now - state.then));
              chart.data.datasets[1].data.push((rxtx[1] - state.last[1]) * 8 / 1000000 / (now - state.then));
              state.last = rxtx;
              state.then = now;
              chart.update();
            }
          });
        })();
      </script>
    `
  }
}
