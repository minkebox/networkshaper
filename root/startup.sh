#! /bin/sh

# Shape the traffic coming from the private interface
/wondershaper.sh -a ${__INTERNAL_INTERFACE} -u ${IN_TO_OUT} -d ${OUT_TO_IN}

# Network dns
dnsmasq

# Monitoring secondary network. We switch TX and RX as anything
# we receive from a service is being transmitted out, and vice versa.
iptables -I OUTPUT -o ${__SECONDARY_INTERFACE} -j RX
iptables -I INPUT -i ${__SECONDARY_INTERFACE} -j TX

# Nat outgoing traffic
iptables -t nat -I POSTROUTING -o ${__DEFAULT_INTERFACE} -j MASQUERADE

# Internet only
if [ "${INTERNET_ONLY}" = "true" ]; then
  iptables -A FORWARD -i ${__INTERNAL_INTERFACE} -d 10.0.0.0/8 -j DROP
  iptables -A FORWARD -i ${__INTERNAL_INTERFACE} -d 172.16.0.0/12 -j DROP
  iptables -A FORWARD -i ${__INTERNAL_INTERFACE} -d 192.168.0.0/16 -j DROP
fi

trap "killall dnsmasq sleep; exit" TERM INT
sleep 2147483647d &
wait "$!"
